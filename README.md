# Walle - Network Firewall Policy Made Simple

Today enterprise companies struggle opening network firewall policies. The main difficulty is the lack of communication
between the development team and the security team. The security team is responsible for the firewall policy. Sometimes, the development team doesn't know the exact complexed policy they need, and thus the security team suffers a lot, being a bottleneck of the company in terms of the number of requests coming from various development teams to only one security team.

Walle is a service for the development teams that enables defining and deploying their firewall policy without any intervention by a third-party.
It is important to say that our project does not compromise security. We simply provide developers with the autonomous capability to
define and deploy their policy without derogating the organization security principles.

**Highlights**

* Ease of use through simply json-based restful API.
* Application authorization implemented to separate between every application rule or object.
* Portable - runs on existing hardware or public/private cloud - Docker Swarm native.
* Shared object feature - which allows applications to interact with each other by publishing their objects.
* Strong json-schema validation with informative error messages.

## Getting Started

These instructions will provide you with a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

1. Docker swarm environment
2. Checkpoint firewall and manager
3. Checkpoint api enabled
4. Walle Rules opened in the firewall


#### 1. Initialize Swarm Mode

You can create a single-host Docker Swarm on your laptop with a single command. You don't need any software additional to Docker 17.06 or greater. You can also run these commands on a Linux VM or cloud host.

This is how you initialize your master node:

```
# docker swarm init
```

If you have more than one IP address you may need to pass a string like `--advertise-addr eth0` to this command.

Take a note of the join token

* Join any workers you need

Log into your worker node and type in the output from `docker swarm init` on the master. If you've lost this info then type in `docker swarm join-token worker` and then enter that on the worker.

It's also important to pass the `--advertise-addr` string to any hosts which have a public IP address.

> Note: check whether you need to enable firewall rules for the [Docker Swarm ports listed here](https://docs.docker.com/engine/swarm/swarm-tutorial/).

#### 2. Installing Checkpoint Firewall

You can get checkpoint firewall and manager on the same machine (all-in-one) iso image by
 1. creating a new account in https://www.checkpoint.com/
 2. go to [checkpoint support center portal](https://supportcenter.checkpoint.com/supportcenter/portal?eventSubmit_doShowproductpage&productTab=overview&product=435)
 3. in Downloads - download R80.10 Fresh Install

If you aren't familiar with checkpoint installation, you can use the [checkpoint documentation file](http://dl3.checkpoint.com/paid/26/26936abfdd6d57c130e6c0d51a624d15/CP_R80.10_Installation_and_Upgrade_Guide.pdf?HashKey=1522154872_ad4fd791b5ff7587d5460aed1e45d97b&xtn=.pdf) to get started.
For standalone installation go to  "Installing a Standalone Server-Gateway Appliance or Open Server"

#### 3. Enable Checkpoint API

Just activate by opening SmartConsole, navigating to Manage & Settings > Blades > Management API > Advanced Settings and changing the settings there.

#### 4. Walle Rules
Walle is a collection of docker services in the same network. There's no need to worry about the internal interactions.

| Source           | Destination         | Port          |
|:----------------:|:-------------------:|:-------------:|
| clients          | docker engine ip    | default: 2493 |
| docker engine ip | firewall manager ip | default: 443  |


### Installing Walle

* clone this git repository to desired directory
* walle is based on a list of environment variables:

| Env Variable                | Default                  |
|:--------------------------- |:------------------------:|
| WALLE_HOST                  | 0.0.0.0                  |
| WALLE_PORT                  | 2493                     |
| WALLE_LOG_LEVEL             | DEBUG                    |
| WALLE_LOG_FILE              | /var/log/walle/walle.log |
| WALLE_REDIS_CONTAINER_NAME  | walle-redis              |
| WALLE_REDIS_PORT            | 6379                     |
| WALLE_RABBIT_CONTAINER_NAME | walle-rabbit             |
* build your images and deploy by running deploy.sh script

## Authors

* **Doron Semama** - *Initial work* - [Gitlab Profile](https://gitlab.com/Semama)

