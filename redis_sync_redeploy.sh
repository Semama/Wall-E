docker service rm walle-redis-sync
docker build -t walle/redis_sync:latest -f Redis_Sync_Dockerfile .
docker push walle/redis_sync
docker service create --name=walle-redis-sync --network=walle --replicas=1 --env REDIS_SERVER=walle-redis walle/redis_sync

