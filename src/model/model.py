import json

from src.checkpoint.checkpoint import Checkpoint
from src.redis.Redis import Redis
from src.checkpoint.entities.network import Network
from src.checkpoint.entities.group import Group
from src.checkpoint.entities.service import Service
from src.checkpoint.entities.access_rule import AccessRule

from src.utils.exceptions import UnknownEntityException

class Model(object):

    def __init__(self, firewall, redis):
        self.fw = firewall
        self.redis = redis

    def cleanup(self, tag):
        for type in Checkpoint.object_types:
            print("Model - cleanup - type: {}".format(type))
            object_list = self.get_objects(tag=tag, type=type)
            if object_list == None or object_list == []:
                continue
            entity = self.get_entity_by_type(type=type)
            print("entity class: {c}".format(c=entity.__class__))
            entity.delete_object_list(object_list)
        self.redis.delete(tag=tag)

    def get_objects(self, tag, type):
        data = None
        try:
            data = self.redis.get_objects(tag=tag, objects_type=type)
            if data == []: raise IOError("tag not found in redis")
        except Exception, e:
            print("Failed using cache. reason: {}".format(e.message))
            print("falling back to use firewall API..")
            try:
                entity = self.get_entity_by_type(type=type)
                print("entity class: {}".format(entity.__class__))
                data = entity.read_all(tag=tag)
            except Exception, e:
                print("Model - get_objects - Exception: {}".format(e.message))
        finally:
            print("type - {type}, tag - {tag} \n data: \n\n {data} \n\n".format(type=type, tag=tag, data=data))
            if isinstance(data, str):
                data = json.loads(data)
            return data

    def get_entity_by_type(self, type):
        print("Model - get_entity_by_type - starting.. type: {}".format(type))
        if type == 'networks':
            entity = self.fw.network
        elif type == 'groups':
            entity = self.fw.group
        elif type == 'services':
            entity = self.fw.service
        elif type == 'access_rules':
            entity = self.fw.access_rule
        else:
            raise UnknownEntityException("unknown entity type: {}".format(type))
        return entity
