import redis
import os
import json

from src.utils.exceptions import TagNotFoundInCache

class Redis(object):

    def __init__(self):
        pool = redis.ConnectionPool(host=os.environ['REDIS_SERVER'], port=6379, db=0)
        self.redis = redis.Redis(connection_pool=pool)

    def create_request(self):
        pass

    def update_request_status(self):
        pass

    def delete_request(self):
        pass

    def create_new_tag(self, tag, value, force=False):
        tag_is_exists = self.redis.exists(tag)
        if force == True and tag_is_exists:
            self.delete(tag=tag)
        if not tag_is_exists:
            self.redis.set(name=tag, value=json.dumps(value))
            return True
        return False

    def update_tag_data(self, tag, value):
        self.redis.set(name=tag, value=json.dumps(value))

    def delete(self, tag):
        if self.redis.exists(tag):
            self.redis.delete(tag)

    def flush_all_fw_data(self):
        keys = self.redis.keys('[^request_]*')
        for key in keys:
            self.redis.delete(key)

    def get_tag(self, tag):
        if not self.redis.exists(tag):
            raise TagNotFoundInCache("tag is not exist")
        return json.loads(self.redis.get(tag))

    def get_objects(self, tag, objects_type):
        tag_data = self.get_tag(tag=tag)
        if objects_type not in tag_data.keys():
            return []
        return tag_data[objects_type]
