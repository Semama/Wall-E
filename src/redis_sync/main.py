import time
import pika
import os

from src.redis_sync.Syncronize import Syncronize

SLEEP_TIME = 60*10

syncronize = Syncronize()

def sync(ch, method, properties, body):
    try:
        syncronize.sync()
    except Exception, e:
        print("sync function - {}".format(e.message))
    finally:
        ch.basic_ack(delivery_tag=method.delivery_tag)

if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=os.environ['RABBIT_SERVER']))
    channel = connection.channel()
    channel.queue_declare(queue='redis_sync_trigger', durable=True)
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(sync, queue='redis_sync_trigger')
    channel.start_consuming()

