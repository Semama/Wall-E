import redis
import json
import os

from src.checkpoint.checkpoint import Checkpoint

class Syncronize(object):

    object_types = Checkpoint.object_types

    def __init__(self):
        self.fw = Checkpoint()
        pool = redis.ConnectionPool(host=os.environ['REDIS_SERVER'], port=6379, db=0)
        self.cache = redis.Redis(connection_pool=pool)

    def sync(self):
        data = self.pull_data_from_fw()
        for key in data.keys():
            self.sync_entity(entity = key, data=data)
        data = self.cached_data_format(data)
        result = self.sync_tags(data)
        return result

    def pull_data_from_fw(self):
        data = {}
        try:
            self.fw.login(read_only=True)
            for type in Syncronize.object_types:
                entity = self.get_entity(type)
                entity_data = entity.read_all()
                data.update({type: entity_data})
        except Exception, e:
            print("Syncronize - pull_data_from_fw - Could not pull data from fw. description: {}".format(e.message))
        finally:
            self.fw.logout()
            return data

    def get_entity(self, obj):
        if obj == 'networks':
            entity = self.fw.network
        elif obj == 'groups':
            entity = self.fw.group
        elif obj == 'services':
            entity = self.fw.service
        elif obj == 'access_rules':
            entity = self.fw.access_rule
        else:
            raise Exception("Unkown Object Type")
        return entity

    def cached_data_format(self, data):
        cached_data = {}
        for type in Syncronize.object_types:
            for obj in data[type]:
                if type == 'access_rules':
                    tag = obj['name']
                else:
                    tags = obj['tags']
                    if len(tags) != 0:
                        tag = tags[0]['name']
                    else:
                        tag = "None"
                if tag not in cached_data.keys():
                    cached_data.update({tag: {}})
                if type not in cached_data[tag].keys():
                    cached_data[tag].update({type: []})
                cached_data[tag][type].append(obj)
        return cached_data

    def sync_tags(self, data):
        for key in self.cache.keys('[^request_]*'):
            self.cache.delete(key)
        for tag in data.keys():
            self.cache.set(name=tag, value=json.dumps(data[tag]))
            #TODO: should be implemented with pipelines and watch
            # if not self.cache.exists(tag):
            #     self.cache.set(name=tag, value=json.dumps(data[tag]))
            # elif self.cache.get(tag) != json.dumps(data[tag]):
            #     self.cache.delete(tag)
            #     self.cache.set(name=tag, value=json.dumps(data[tag]))
        return True

    def sync_entity(self, entity, data):
        key = "_" + entity
        if key in self.cache.keys():
            self.cache.delete(key)
        self.cache.set(name=key, value = data[entity])


