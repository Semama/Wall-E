import os
from os import environ as ENV
from logging import getLogger
from configparser import ConfigParser
from definitions import CONFIG_PATH

logger = getLogger(__name__)
logDir = '/var/log'

confFileOrder = [ CONFIG_PATH ]
cp = ConfigParser()
cp.read(filenames = confFileOrder)

try:
    logfile = cp.get(section='default', option='logfile_Windows')
except Exception:
    logger.debug("couldn't get logfile name. falling back to  default")
    if not os.path.exists(logDir):
        os.makedirs(logDir)
    logfile = logDir + '/checkpoint_auto.log'

try:
    loglevel = cp.get(section='default', option='loglevel')
except Exception:
    logger.debug("couldn't get log level. falling back to  default")
    loglevel = 'DEBUG'

try:
    host = cp.get(section='server', option='host')
    port = cp.get(section='server', option='port')
except Exception:
    logger.debug("couldn't get bind address. falling back to  default")
    host = '0.0.0.0'
    port = '2493'

host = ENV.get('WALLE_HOST', '0.0.0.0')
port = ENV.get('WALLE_PORT', '2493')
loglevel = ENV.get('WALLE_LOG_LEVEL', 'DEBUG')
logfile = ENV.get('WALLE_LOG_FILE', '/var/log/walle/walle.log')
redis_port = ENV.get('WALLE_REDIS_PORT', '6379')
