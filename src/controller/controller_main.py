import logging

from src.controller.controller import CheckpointController
from src.configs import *

if __name__ == '__main__':
    logging.getLogger(__name__)
    logging.basicConfig(filename=logfile,
                            format='[%(asctime)s] [%(levelname)s] %(module)s - %(funcName)s:   %(message)s',
                            level=loglevel,
                            datefmt='%m/%d/%Y %I:%M:%S %p')
    ctrl = CheckpointController()
    ctrl.consume_requests()