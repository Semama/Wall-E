import os
import json
import pika
import redis

from src.checkpoint.checkpoint import Checkpoint
from src.controller.request_handler import RequestHandler
from src.utils.entities.request import Request
from src.redis.Redis import Redis


class CheckpointController(object):

    firewall = Checkpoint()
    pool = redis.ConnectionPool(host=os.environ['REDIS_SERVER'])
    red = redis.Redis(connection_pool=pool)


    def _get_rules(self):
        #TODO
        pass

    def _get_network_objects(self):
        #TODO
        pass

    def _get_services(self):
        #TODO
        pass

    def get_all(self):
        #TODO
        pass

    @staticmethod
    def execute_request(ch, method, properties, body):
        body = json.loads(body)
        created_time = body['created_time']
        status = body['status']
        body = body['request']
        req = Request(body=body)
        req.created_time = created_time
        req.status = status
        CheckpointController.firewall.login()
        try:
            #TODO: save request id, write to redis
            request_handler = RequestHandler(req.body, CheckpointController.firewall, Redis())
            request_handler.clean_project()
            request_handler.implement_request()
            CheckpointController.firewall.install_policy(fix_errors=True)
            req.status = "Done"
        except Exception, e:
            print("Exception: {}".format(e.message))
            CheckpointController.firewall.policy.discard()
            req.description = e.message
            req.status = "Failed"
        finally:
            CheckpointController.firewall.logout()
            CheckpointController.red.set(name=req.get_key(), value=str(req))
            ch.basic_ack(delivery_tag=method.delivery_tag)
            return req.status

    def consume_requests(self):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=os.environ['RABBIT_SERVER']))
        channel = connection.channel()
        channel.queue_declare(queue='checkpoint_requests', durable=True)
        channel.basic_qos(prefetch_count=1)
        channel.basic_consume(self.execute_request,
                              queue='checkpoint_requests')
        channel.start_consuming()