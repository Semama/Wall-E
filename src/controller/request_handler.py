import ipaddress
import redis

from src.utils.network_object_manipulation import NetworkObjectManipulation as Validate
from src.utils.exceptions import *
from src.redis import Redis
from src.model.model import Model


class RequestHandler(object):

    def get_names_from_list(self, list):
        names = []
        for obj in list:
            names.append(obj['name'])
        return names

    def _get_project(self, data):
        return data['project']

    def _get_networks(self, data):
        network_names = []
        if 'networks' not in data.keys():
            return network_names
        networks = data['networks']
        for network in networks:
            ipv4_network = network['subnet'] + "/" + str(network['mask'])
            is_network = Validate.is_network(ipv4_network)
            if not is_network:
                raise ipaddress.NetmaskValueError("Invalid Network: network '{}' is invalid".format(network['name']))
            network_names.append({
                'name': network['name'],
                'subnet': network['subnet'],
                'mask': Validate.get_subnet_mask_from_network(network['subnet'] + "/" + str(network['mask']))
            })
        return network_names

    def _get_hosts(self, data):
        host_names = []
        if 'hosts' not in data.keys():
            return host_names
        hosts = data['hosts']
        for host in hosts:
            address = host['ip']
            is_valid = Validate.is_ip_address(address) or Validate.is_dns_name(address)
            if not is_valid:
                raise ipaddress.AddressValueError("Invalid IPv4 Address: ip '{}' is invalid".format(address))
            address.decode()
            host_names.append({
                'name': host['name'],
                'subnet': address,
                'mask': Validate.get_subnet_mask_from_network(address + "/32")
            })
        return host_names

    def _get_groups(self, data):
        groups = []
        if 'groups' not in data.keys():
            return groups
        for group in data['groups']:
            if 'networks' in group.keys():
                if self.networks == []:
                        raise InvalidGroupException("Invalid Request. can't create group that contains networks without define networks")
                net_names = self.get_names_from_list(self.networks)
                for network in group['networks']:
                    if network not in net_names:
                        raise UnknownEntityException("Unkown network name: {}".format(network))
            if 'hosts' in group.keys():
                if self.hosts == []:
                        raise InvalidGroupException("Invalid Request. can't create group that contains hosts without define hosts")
                for host in group['hosts']:
                    host_names = self.get_names_from_list(self.hosts)
                    if host not in host_names:
                        raise UnknownEntityException("Unkown host name: {}".format(host))
            hosts = group['hosts'] if 'hosts' in group.keys() else []
            networks = group['networks'] if 'networks' in group.keys() else []
            members = hosts + networks
            groups.append({
                'name': group['name'],
                'members': members
            })
        return groups

    def _get_services(self, data):
        print("_get_services")
        if 'services' not in data.keys():
            return []
        for service in data['services']:
            port = service['port']
            protocol = service['protocol']
            if not Validate.is_valid_port(port):
                raise PortOutOfRange("Invalid Port Number: {}".format(port))
            if not Validate.is_valid_protocol(protocol):
                raise InvalidProtocol("Invalid Protocol. should be 'tcp' or 'udp'. given: {}".format(protocol))
        return data['services']

    def _get_rules(self, data):
        print("_get_rules")
        if 'rules' not in data.keys():
            return []
        rules = data['rules']
        for rule in rules:
            destinations = rule['dest']
            sources = rule['src']
            services = rule['services']
            hosts = self.get_names_from_list(self.hosts)
            networks = self.get_names_from_list(self.networks)
            groups = self.get_names_from_list(self.groups)
            service_names = self.get_names_from_list(self.services)
            valid_endpoints = hosts + networks + groups
            for source in sources:
                if source not in valid_endpoints:
                    raise UnknownEntityException("Unknown Source: {}".format(source))
            for destination in destinations:
                if destination not in valid_endpoints:
                    raise UnknownEntityException("Unknown Destination: {}".format(destination))
            for service in services:
                if service not in service_names:
                    raise UnknownServiceException("Unknown Service: {}".format(services))
        return rules

    def __init__(self, json_file, firewall, redis):
        self.project = self._get_project(json_file)
        self.networks = self._get_networks(json_file)
        self.hosts = self._get_hosts(json_file)
        self.groups = self._get_groups(json_file)
        self.services = self._get_services(json_file)
        self.rules = self._get_rules(json_file)
        self.fw = firewall
        self.redis = redis
        self.model = Model(firewall = self.fw, redis=self.redis)

    def clean_project(self):
        print("cleanup")
        self.model.cleanup(self.project)
        print("finished cleanup")

    def _create_endpoints(self, endpoints):
        networks = []
        for endpoint in endpoints:
            name = endpoint['name']
            subnet = endpoint['subnet']
            mask = endpoint['mask']
            tag = self.project
            data = self.fw.network.create(name=name, subnet=subnet, mask=mask, tag=tag)
            networks.append(data)
        return networks

    def create_networks(self):
        return self._create_endpoints(endpoints=self.networks)

    def create_hosts(self):
        return self._create_endpoints(endpoints=self.hosts)

    def create_groups(self):
        groups = []
        for group in self.groups:
            data = self.fw.group.create(name=group['name'], members=group['members'], tag=self.project)
            groups.append(data)
        return groups

    def create_services(self):
        services = []
        for service in self.services:
            data = self.fw.service.create(name=service['name'], port=service['port'], protocol=service['protocol'], tag=self.project)
            services.append(data)
        return services

    def create_rules(self):
        rules = []
        for rule in self.rules:
            data = self.fw.access_rule.create(source=rule["src"], destination=rule["dest"], service=rule["services"], name=self.project)
            rules.append(data)
        return rules

    def implement_request(self):
        data = {}
        networks = self.create_networks()
        hosts = self.create_hosts()
        if networks != [] or hosts != []:
            data.update({'networks': networks})
            data['networks'] += hosts
        groups = self.create_groups()
        if groups != []:
            data.update({'groups': groups})
        services = self.create_services()
        if services != []:
            data.update({'services': services})
        rules = self.create_rules()
        if rules != []:
            data.update({'access_rules': rules})
        self.redis.create_new_tag(tag=self.project, value=data)