import time

from src.checkpoint.entities.access_rule import AccessRule
from src.checkpoint.entities.group import Group
from src.checkpoint.entities.network import Network
from src.checkpoint.entities.policy import Policy
from src.checkpoint.entities.section import Section
from src.checkpoint.entities.service import Service
from src.checkpoint.entities.session import Session

from src.checkpoint.entities.tag import Tag

KEEP_ALIVE_SECONDS = 9*60
#TODO: Logger !!

HIDDING_RULE = 0
HIDDEN_RULE = 1

class Checkpoint(object):

    object_types = ['access_rules', 'groups', 'networks', 'services']

    def __init__(self, policy_name='Standard', policy_layer='Network', target='FW-DEV'):
        self.session = Session()
        self.url = self.session.url
        self.token = None
        self.endpoint = None
        self.policy_name = policy_name
        self.policy_layer = policy_layer
        self.policy_targets = target
        self.policy = None
        self.access_rule = None
        self.network = None
        self.section = None
        self.tag = None
        self.service = None
        self.group = None

    def login(self, read_only=False):
        self.session.login(read_only=read_only)
        self.token = self.session.get_token()
        self.policy = Policy(api_url=self.url, token=self.token, policy=self.policy_name, targets=self.policy_targets)
        self.access_rule = AccessRule(api_url=self.url, token=self.token, target = self.policy_targets, layer= self.policy_layer)
        self.network = Network(api_url=self.url, token=self.token)
        self.section = Section(api_url=self.url,token=self.token)
        self.tag = Tag(api_url=self.url,token=self.token)
        self.service = Service(api_url=self.url,token=self.token)
        self.group = Group(api_url=self.url,token=self.token)
        return True

    def install_policy(self, fix_errors=False):
        task_id = self.policy.verify()['task-id']
        self.session.refresh()
        result = self._get_task_result(task_id=task_id)
        if result['status'] == 'failed':
            if fix_errors:
                self.fix_policy_errors(task_id=task_id, fixed_pair=[0,0])
            else:
                return result
        task_id = self.policy.install()['task-id']
        self.session.refresh()
        return self._get_task_result(task_id=task_id)

    def fix_policy_errors(self, task_id, fixed_pair):
        result = self._get_task_result(task_id=task_id)
        if result['status'] != 'failed':
            return True
        pair = self._get_list_of_nook_rules(result)[0]
        if pair == fixed_pair:
            return False
        hidding_rule = self.access_rule.read_by_rule_number(int(pair[HIDDING_RULE]))
        hidden_rule = self.access_rule.read_by_rule_number(int(pair[HIDDEN_RULE]))
        self.access_rule.update(uid=hidden_rule['uid'], new_position=int(pair[HIDDING_RULE]))
        task_id = self.policy.verify()['task-id']
        self.session.refresh()
        result = self._get_task_result(task_id=task_id)
        if result['status'] == 'failed':
            swapped_pair = self._get_list_of_nook_rules(result)[0]
            if swapped_pair[HIDDING_RULE] == pair[HIDDING_RULE] and swapped_pair[HIDDEN_RULE] == pair[HIDDING_RULE] +1:
                self.access_rule.update(rule_number=swapped_pair[HIDDEN_RULE], enabled=False)
                self.access_rule.update(rule_number=swapped_pair[HIDDING_RULE], comments=hidding_rule['uid'])
        task_id = self.policy.verify()['task-id']
        self.session.refresh()
        self.fix_policy_errors(task_id=task_id, fixed_pair=pair)

    def _get_names_from_object_list(self, list):
        names = []
        for obj in list:
            names.append(obj['name'])
        return names

    def _get_task_result(self, task_id):
        keep_waiting = True
        while keep_waiting:
            task = self.policy.show_task(task_id=task_id)['tasks'][0]
            if 'status' not in task:
                print("something wrong - result: \n{}".format(task))
                return task
            if task['status'] == 'in progress':
                time.sleep(0.5)
                continue
            keep_waiting = False
        return task

    def _get_list_of_nook_rules(self, task_result):
        if 'task-details' not in task_result:
            raise ValueError("bad parameter. _get_list_of_nook_rules gets policy installation or verification result")
        task_details = task_result['task-details']
        list_of_messages = []
        for task_detail in task_details:
            if 'stagesInfo' in task_detail:
                list_of_messages = self._get_install_policy_messages(task_detail)
            if 'errors' in task_detail:
                list_of_messages = self._get_verify_policy_messages(task_detail)
            pair_of_nook_rules = []
            for message in list_of_messages:
                if 'Hides' in message:
                                pair = self._get_nook_rules(message=message)
                                pair_of_nook_rules.append(pair)
        return pair_of_nook_rules

    def _get_install_policy_messages(self, task_details):
        message_details = []
        for gateway in task_details:
            if gateway['gatewayName'] == self.policy_targets:
                for message in gateway['stagesInfo'][0]['messages']:
                    message_details = message['message'].split('\n')
        return message_details

    def _get_verify_policy_messages(self, task_details):
        return task_details['errors']

    def _get_nook_rules(self, message):
        hidding_rule = int(message.split('Rule ')[1].split(' Hides ')[0])
        hidden_rule = int(message.split('rule ')[1].split(' for')[0])
        return [hidding_rule, hidden_rule]

    def logout(self):
        self.session.logout()
        return True
