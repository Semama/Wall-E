
from src.checkpoint.entities.entity import Entity

class Group(Entity):
    def __init__(self,api_url, token):
        super(Group,self).__init__(url=api_url, token=token)

    def create(self, name, members, tag):
        data = {
            'name': name,
            'members': members,
            'tags': tag,
            'ignore-warnings': True
        }
        return self.request(self.get_url('add-group'), data=data)

    def read(self, name):
        data = {
            'name': name
        }
        return self.request(self.get_url('show-group'), data=data)

    def _read_all(self, limit=50, offset=0):
        data = {
            'limit':limit,
            'offset': offset,
            'details-level': 'full'
        }
        r = self.request(self.get_url('show-groups'), data=data)
        return r['objects']

#    def read_all(self):
 #       counter = 0
  #      bulk = 50
   #     objects=[]
    #    chunk = ["temp string"]
     #   while len(chunk) > 0:
 #           chunk = self._read_all(limit=bulk, offset=counter)
 #           objects += chunk
  #          counter += len(chunk)
   #     return objects

    def update(self, name, content, tag):
        data = {
            'name': name,
            'members': content,
            'tags': tag
        }
        return self.request(self.get_url('set-group'), data=data)

    def delete(self, group):
        name = group['name']
        data = {
            'name': name,
            'ignore-warnings': True
        }
        return self.request(self.get_url('delete-group'), data=data)

