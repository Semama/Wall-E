
from src.checkpoint.entities.entity import Entity
from src.utils.exceptions import UnknownServiceException

class Service(Entity):

    def __init__(self, api_url, token):
        super(Service,self).__init__(url=api_url, token=token)

    def create(self, tag, name, protocol, port):
        url = self._get_url_by_protocol_and_command(protocol=protocol, command='add')
        data = {
            'name': name,
            'port': str(port),
            'tags': tag,
            'ignore-warnings': True
        }
        return self.request(url=url, data=data)

    def read(self, name, protocol):
        data = {
            'name': name
        }
        url = self._get_url_by_protocol_and_command(protocol=protocol, command='show')
        return self.request(url=url, data=data)

    def _read_all(self, limit=50, offset=0):
        objs = self._read_all_proto_services(udp_or_tcp='tcp', limit=limit, offset=offset)
        objs += self._read_all_proto_services(udp_or_tcp='udp', limit=limit, offset=offset)
        return objs

    def _read_all_proto_services(self, udp_or_tcp, limit=50, offset=0):
        data = {
            'limit': limit,
            'offset': offset,
            'details-level': 'full'
        }
        if udp_or_tcp == 'tcp':
            r = self.request(self.get_url('show-services-tcp'), data=data)
        elif udp_or_tcp == 'udp':
            r = self.request(self.get_url('show-services-udp'), data=data)
        else:
            raise Exception("Service - _read_all: bad protocol")
        return r['objects']

    def _read_all_pages(self, proto):
        counter = 0
        bulk = 50
        objects=[]
        chunk = ["temp string"]
        while len(chunk) > 0:
            chunk = self._read_all_proto_services(udp_or_tcp=proto, limit=bulk, offset=counter)
            objects += chunk
            counter += len(chunk)
        return objects

    def read_all_names(self, tag):
        objects = self._read_all_pages('tcp') + self._read_all_pages('udp')
        list_of_object_names = []
        for object in objects:
            name = object['name']
            tags = object['tags']
            if len(tags) != 0:
                tags = tags[0]
                if tags['name'] == tag:
                    list_of_object_names.append(name)
        return list_of_object_names

    def update(self,name,tag):
        [protocol, port] = Service._extract_protocol_and_port_from_service_name(name)
        url = self._get_url_by_protocol_and_command(protocol=protocol, command='set')
        data =  {
            'name': name,
            'tags': tag
        }
        return self.request(url=url, data=data)

    def delete(self, service):
        service_type = service['type']
        name = service['name']
        if service_type == 'service-udp':
            protocol = 'udp'
        elif service_type == 'service-tcp':
            protocol = 'tcp'
        else:
            raise UnknownServiceException()
        r = self.read(name, protocol=protocol)
        if r is None:
            raise Exception("object \'{name}\' is not found".format(name=name))
        url = self._get_url_by_protocol_and_command(protocol=protocol, command='delete')
        data = {
            'name': name,
            'ignore-warnings': True
        }
        return self.request(url=url, data=data)

    @staticmethod
    def _extract_protocol_and_port_from_service_name(name):
        if name.count('/') != 1:
           raise Exception("Bad Request. invalid service: {}".format(name))

        [protocol, port] = name.split('/')
        if protocol != 'tcp' and protocol != 'udp':
            raise Exception("Bad Request. invalid protocol: {}".format(protocol))
        if port < 1 or port > 65535:
            raise Exception("Bad Request. invalid port number: {}".format(port))
        return [protocol, port]

    def _get_url_by_protocol_and_command(self, protocol, command):
        if protocol == 'tcp':
            return self.get_url('{command}-service-tcp'.format(command=command))
        if protocol == 'udp':
            return self.get_url('{command}-service-udp'.format(command=command))
        raise Exception("Bad Request. invalid protocol: {}".format(protocol))