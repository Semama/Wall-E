from src.checkpoint.entities.entity import Entity

from src.checkpoint.entities.service import Service


class AccessRule(Entity):

    def __init__(self,api_url, token, target, layer):
        super(AccessRule,self).__init__(url=api_url, token=token)
        self.layer = layer
        self.target = target
        self.service = Service(api_url=api_url, token=token)

    def create(self, source, destination, service, name, position='top'):
        data = self.access_rule_format(source, destination, service ,name, position)
        return self.request(self.get_url('add-access-rule'), data=data)

#TODO: duplicate code.

    def read(self, uid=None):
        if uid is None:
            data = {
                'name': 'created by Jess',
                'layer': self.layer,
                'details-level': 'full'
            }
        else:
            data = {
                'uid': uid,
                'layer': self.layer,
                'details-level': 'full'
            }
        return self.request(self.get_url('show-access-rule'), data=data)

    def read_by_rule_number(self, rule_number):
        if rule_number is None:
            data = {
                'name': 'created by Jess',
                'layer': self.layer,
                'details-level': 'full'
            }
        else:
            data = {
                'rule-number': rule_number,
                'layer': self.layer,
                'details-level': 'full'
            }
        return self.request(self.get_url('show-access-rule'), data=data)

    def read_all(self, tag=None):
            counter = 0
            bulk = 50
            rules = []
            chunk = ['Temp String']
            while len(chunk) > 0:
                chunk = self._read_all(tag=tag, limit=bulk, offset=counter)
                rules += chunk
                counter += bulk
            if tag:
                rules = self._match_exact_rule_name(rule_name=tag, rules=rules)
            return rules

    def _read_all(self, tag=None, limit=50, offset=0):
        url = self.get_url('show-access-rulebase')
        if not tag:
            data = {
                'name': self.layer,
                'limit': limit,
                'offset': offset,
                'details-level': 'full'
            }
        else:
            data = {
                'name': self.layer,
                'filter': tag,
                'limit': limit,
                'offset': offset,
                'details-level': 'full'
            }
        r = self.request(url=url, data=data)
        rules = self._get_rules_from_rulebase(r['rulebase'])
        return rules

    def _match_exact_rule_name(self, rule_name, rules):
        filtered_rules = []
        for rule in rules:
            if rule['name'] == rule_name:
                filtered_rules.append(rule)
        return filtered_rules

    def _get_rules_from_rulebase(self, rulebase):
        rules = []
        try:
            for obj in rulebase:
                if 'rulebase' in obj.keys():
                    rules += self._get_rules_from_rulebase(obj['rulebase'])
                else:
                    rules.append(obj)
        except Exception,e:
            print("Access_rule - _get_rules_from_rulebase - Exception: {}".format(e.message))
            raise Exception(e.message)
        return rules

    def read_all_rule_numbers(self, tag, limit=50, offset=0):
        rulebase = self.read_all(tag=tag, limit=limit, offset=offset)
        rule_numbers = []
        for i in range(0, len(rulebase)):
            rule = rulebase[i]
            rule_numbers.append(rule['rule-number'])
        return rule_numbers

    def read_all_names(self, tag, limit=50, offset=0):
        return self.read_all_rule_numbers(tag=tag, limit=limit, offset=offset)

    def update(self, uid=None, source=None, destination=None, rule_number=None, service=None, position=None, new_position=None, comments=None, name=None, enabled=True):
        data = {'layer': self.layer}
        if uid: data.update({'uid':uid})
        if rule_number: data.update({'rule-number': str(rule_number)})
        if source: data.update({'source': source})
        if destination: data.update({'destination': destination})
        if service: data.update({'service': service})
        if position: data.update({'position': position})
        if name: data.update({'name': name})
        if new_position: data.update({'new-position': new_position})
        if comments: data.update({'comments': comments})
        data.update({'enabled': enabled})
        return self.request(self.get_url('set-access-rule'), data=data)

    def delete(self, rule):
        print("AccessRule - delete - {}".format(rule['name']))
        uid = rule['uid']
        data = {
            'uid': uid,
            'layer' : self.layer
        }
        return self.request(self.get_url('delete-access-rule'), data=data)

    def access_rule_format(self, source, destination, service, name, position, action='Accept', track='Log', enabled=True):
        data = {
            'layer': self.layer,
            'position': position,
            'name': name,
            'source': source,
            'destination': destination,
            'service': service,
            'track': track,
            'install-on': self.target,
            'action': action,
            'ignore-warnings': True,
            'enabled': enabled
        }
        return data

    def delete_object_list(self, rules):
        print("Entity - delete_object_list - access_rules")
        for rule in rules:
            if 'comments' in rule.keys() and rule['comments'] != "":
                self.update(uid=rule['comments'], enabled=True)
            print("deleting object name: {}".format(rule['name']))
            self.delete(rule)

class UnexistingObject(Exception):
    pass