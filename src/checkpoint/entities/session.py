import json
import time
import requests

#from src.utils.stoppable_thread import StoppableThread
from logging import getLogger

logger = getLogger(__name__)

KEEP_ALIVE_SECONDS = 3

class Session(object):

    # Decorator
    def login_required(func):
        def func_wrapper(self, **kwargs):
            if not self.is_logged_in():
                raise Exception({'code' : 'not_logged_in', 'message':'Please Login before calling \'{}\''.format(func.__name__)})
            return func(self, **kwargs)
        return func_wrapper


    def __init__(self, name='Jess Session', description='Jess Session Created by Checkpoint SDK', management='10.20.30.40'):
        self.name = name
        self.description = description
        self.management = management
        self.port = '443'
        self.url = 'https://{server}:{port}/web_api/'.format(server=self.management, port=self.port)
        self.session_details = None
        self.token = None
        #self.token_management = StoppableThread(target=self.token_auto_renewal)
        self._logged_in = False

    def login(self, read_only=False):
        if self.token != None:
            return True
        headers = {'Content-Type': 'application/json'}
        user = 'api_admin'
        password = 'g5HKRD687jfvkfr369540hndjbfr09756'
        if read_only:
            data = {
                'user': user,
                'password': password,
                'read-only': True
            }
        else:
            data = {
                'user': user,
                'password': password,
                'session-name': self.name,
                'session-description': self.description,
            }
        r = requests.post(self.get_url('login'), headers=headers, json=data, verify=False)
        if r.status_code != 200:
            raise Exception(json.loads(r.text))
        self.session_details = json.loads(r.text)
        self.token = self.session_details['sid']
        #self.token_management.start()
        self._logged_in = True
        return True

    @login_required
    def refresh(self):
        data = {
            'description': self.description,
            'new-name': self.name
        }
        return self.request(self.get_url('set-session'), data=data)

    @login_required
    def logout(self):
        #self.token_management.stop()
        headers = {'Content-Type': 'application/json',
                   'X-chkp-sid': self.get_token()}
        r = self.request(self.get_url('logout'), headers)
        message = json.loads(r.text)
        message = message['message']
        if message != 'OK':
            raise Exception(json.loads(message))
        self._logged_in = False
        self.token = None
        return True

    @login_required
    def show_session(self):
        if self.is_logged_in():
            raise Exception("Please Login First!")
        if 'sid' not in self.session_details:
            raise Exception("There is no active session. please log in first")
        headers = {'Content-Type': 'application json',
                   'X-chkp-sid': self.token}
        data = {'uid': self.session_details['uid']}
        r = self.request(self.get_url('show-session'), headers, data)
        return json.loads(r.text)

    @login_required
    def get_token(self):
        return self.token

    @login_required
    def keepalive(self):
        token = self.get_token()
        headers = {'Content-Type': 'application json', 'X-chkp-sid': token}
        self.request(self.get_url('keepalive'), headers)

    def token_auto_renewal(self):
        while True:
            try:
                time.sleep(KEEP_ALIVE_SECONDS)
                self.keepalive()
            except Exception, e:
                if isinstance(e.message, str):
                    message = json.loads(e.message)
                elif isinstance(e.message, dict):
                    message = e.message
                if 'code' not in message.keys():
                    raise Exception("Internal Error")
                if message['code'] == 'generic_err_wrong_session_id':
                    return
                if message['code'] == 'not_logged_in':
                    return
                raise Exception("Internal Error")

    def get_url(self, path):
        return self.url + path

    def request(self, url, headers={}, data={}):
        req_headers = {'Content-Type': 'application json', 'X-chkp-sid': self.token}
        for key in headers.keys():
            req_headers[key] = headers[key]
        resp = requests.post(url, headers=req_headers, json=data, verify=False)
        if resp.status_code != 200:
            raise Exception(resp.text)
        return resp

    def is_logged_in(self):
        return self._logged_in

