import requests
import os
import json

from src.utils.exceptions import RequestFailed
from src.utils.exceptions import TagNotFoundInCache


class Entity(object):

    def __init__(self,token, url):
        self.token = token
        self.url = url

    def request(self, url, headers={}, data={}):
        req_headers = {'Content-Type': 'application json', 'X-chkp-sid': self.token}
        for key in headers.keys():
            req_headers[key] = headers[key]
        resp = requests.post(url, headers=req_headers, json=data, verify=False)
        if resp.status_code != 200:
            raise RequestFailed(resp.text)
        return json.loads(resp.text)

    def get_url(self, path):
        return self.url + path

    def read_all(self, tag=None):
        counter = 0
        bulk = 50
        objects=[]
        chunk = ["temp string"]
        while len(chunk) > 0:
            chunk = self._read_all(limit=bulk, offset=counter)
            objects += chunk
            counter += bulk
        if tag != None:
            list_of_object = []
            for object in objects:
                tags = object['tags']
                if len(tags) != 0 and tags[0]['name'] == tag:
                    list_of_object.append(object)
            objects = list_of_object
        return objects

    def read_all_names(self, tag):
        objects = self.read_all(tag=tag)
        list_of_object_names = []
        for object in objects:
            list_of_object_names.append(object['name'])
        return list_of_object_names

    def delete_object_list(self, object_list):
        print("Entity - delete_object_list - starting..")
        for object in object_list:
            print("deleting object name: {}".format(object['name']))
            self.delete(object)

    def _read_all(self):
        raise NotImplementedError("every entity should implement '_read_all' function")

    def delete(self, object):
        raise NotImplementedError("every entity should implement 'delete' function")