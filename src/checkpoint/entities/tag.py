
from src.checkpoint.entities.entity import Entity

class Tag(Entity):

    def __init__(self,api_url, token):
        super(Tag,self).__init__(url=api_url, token=token)



    def create(self, name):
        data = {'name':name}
        return self.request(self.get_url('add-tag'), data=data)

    def read(self, name):
        data = {'name':name}
        return self.request(self.get_url('show-tag'), data=data)

    def _read_all(self, limit=50, offset=0):
        data = {
            'limit':limit,
            'offset': offset
        }
        r = self.request(self.get_url('show-tags'), data=data)
        return r['objects']

    def update(self, name, newName):
        data = {
            'name':name,
            'new-name':newName,

        }
        return self.request(self.get_url('set-tag'), data=data)

    def delete(self, uid):
        data = {'uid': uid}
        return self.request(self.get_url('delete-tag'), data=data)