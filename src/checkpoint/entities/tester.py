import time
from src.checkpoint.checkpoint import Checkpoint

cp = Checkpoint()
try:
    cp.login()

except Exception, e:
    print(e)
    cp.policy.discard()
finally:
    cp.logout()
