import time

from src.checkpoint.entities.entity import Entity

class Policy(Entity):

    def __init__(self,api_url, token, policy, targets):
        super(Policy,self).__init__(url=api_url, token=token)
        self.policy = policy
        self.targets = targets

    def _publish_and_wait_for_result(self):
        resp = self.publish()
        task_id = resp['task-id']
        status = 'in progress'
        #TODO: fix the infinite loop when publish is really failed
        while status == 'in progress' or status == 'failed':
            if status == 'failed':
                task_id = self.publish()['task-id']
                time.sleep(0.5)
            result = self.show_task(task_id=task_id)
            status = result['tasks'][0]['status']
        return status

    def install(self, prepare_only=False, access=True):
        self._publish_and_wait_for_result()
        data = {
            'policy-package': self.policy,
            'access': access,
            'threat-prevention': False,
            'targets': self.targets,
            'prepare-only': prepare_only
        }
        return self.request(self.get_url('install-policy'), data=data)

    def verify(self):
        self._publish_and_wait_for_result()
        data = {
            'policy-package': self.policy
        }
        return self.request(self.get_url('verify-policy'), data=data)

    def publish(self):
        return self.request(self.get_url('publish'))

    def discard(self):
        return self.request(self.get_url('discard'))

    def show_task(self, task_id):
        data = {
            'task-id': task_id,
            'details-level': 'full'
        }
        return self.request(self.get_url('show-task'), data=data)






