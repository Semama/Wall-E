from src.checkpoint.entities.entity import Entity

class Section(Entity):

    def __init__(self,api_url, token, layer='Integration Network', position='top'):
        super(Section,self).__init__(url=api_url, token=token)
        self.layer = layer
        self.position = position

    def create(self, name):
        data = {
            'layer':self.layer,
            'position':self.position,
            'name':name}
        return self.request(self.get_url('add-access-section'), data=data)

    def read(self, name):
        data = {
                'name': name,
                'layer': self.layer
            }
        return self.request(self.get_url('show-access-section'), data=data)

    def update(self, name, uid=None):
        data = {}
        if uid is not None:
            data['uid']=uid
        else:
            data['name']='created by Jess'
        return self.request(self.get_url('set-access-section'), data=data)

    def delete(self, uid):
        data = {'uid': uid}
        return self.request(self.get_url('delete-access-section'), data=data)