from src.checkpoint.entities.entity import Entity

class Network(Entity):
    def __init__(self,api_url, token):
        super(Network,self).__init__(url=api_url, token=token)

    def create(self, tag, name, subnet, mask):
        #TODO: handle subnet-mask & mask-length options.
        data = {
            'name': name,
            'subnet': subnet,
            'subnet-mask': mask,
            'tags': tag,
            'ignore-warnings': True
        }
        return self.request(self.get_url('add-network'), data=data)

    def read(self, name):
        data = {
            'name': name
        }
        return self.request(self.get_url('show-network'), data=data)

    def _read_all(self, limit=50, offset=0):
        data = {
            'limit': limit,
            'offset': offset,
            'details-level': 'full'
        }
        r = self.request(self.get_url('show-networks'), data=data)
        return r['objects']

    def update(self, name, subnet, mask_length,tag):
        data = {
            'name': name,
            'subnet': subnet,
            'mask-length': mask_length
        }
        if tag is not None:
            data['tags'] = tag
        return self.request(self.get_url('set-network'), data=data)

    def delete(self, network):
        print("Network - delete - starting..")
        data = {
            'name': network['name'],
            'ignore-warnings': True
        }
        return self.request(self.get_url('delete-network'), data=data)



