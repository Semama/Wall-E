import json
import datetime

class Request(object):

    request_format = 'request_'

    def __init__(self, body, wrap=False):
        print "Creating Request Object"
        if isinstance(body, str):
            body = json.loads(body)
        self.id = str(body['project'])
        if '_' in self.id:
            raise ValueError('Project name should not contain underscores (\'_\')')
        if self.id == "":
            raise ValueError('Project name should not be empty')
        self.created_time = datetime.datetime.now()
        self.body = body if not wrap else Request.wrap_entities_with_project_name(self.id, body=body)
        self.status = "Pending"
        self.description = None

    def __str__(self):
        request = {
            "request": self.body,
            "status": self.status,
            "created_time": str(self.created_time)
        }
        if self.description != None:
            request.update({"description": self.description})
        return json.dumps(request)

    def get_key(self):
        return Request.request_format + str(self.id)

    @staticmethod
    def wrap_entities_with_project_name(project_name, body):
        return Request._edit_entities_names(project_name=project_name, body=body, func=Request._wrap)

    @staticmethod
    def unwrap_entities_from_project_name(project_name, body):
        return Request._edit_entities_names(project_name=project_name, body=body, func=Request._unwrap)

    @staticmethod
    def _edit_entities_names(project_name, body, func):
        # TODO: REFACTOR !!!! Wrapping all object names and rule parameters (source, destination, service)
        if isinstance(body, str):
            body = json.loads(body)
        for key in body.keys():
            if key == 'project': continue
            elif key == 'groups' or key == 'rules': body[key] = Request._handle_groups_or_rules(project_name, body[key], func)
            else: body[key] = Request._handle_entities(project_name, body[key], func)
        return body

    @staticmethod
    def _handle_groups_or_rules(project_name, list, func):
        for index in range(len(list)):
            if isinstance(list[index], dict):
                for key in list[index].keys():
                    if key == 'name':
                        list[index]['name'] = func(project_name, list[index]['name'])
                    else:
                        for index2 in range(len(list[index][key])):
                            list[index][key][index2] = func(project_name, list[index][key][index2])
        return list

    @staticmethod
    def _handle_entities(project_name, entities, func):
        for index in range(len(entities)):
                    if isinstance(entities[index], dict) and 'name' in entities[index].keys():
                        entities[index]['name'] = func(project_name, entities[index]['name'])
        return entities

    @staticmethod
    def _wrap(project_name, name):
        return project_name + "_" + name

    @staticmethod
    def _unwrap(project_name, name):
        return name.replace(project_name + "_", "", 1)


