import ipaddress
import socket

class NetworkObjectManipulation(object):

    @staticmethod
    def is_ip_address(str):
        str = str.decode()
        result = True
        try:
            ipaddress.ip_address(str)
        except:
            result = False
        finally:
            return result

    @staticmethod
    def is_network(str):
        str = str.decode()
        result = True
        try:
            ipaddress.ip_network(str)
        except:
            result = False
        finally:
            return result

    @staticmethod
    def get_subnet_mask_from_network(net):
        if NetworkObjectManipulation.is_network(net):
            ip = ipaddress.IPv4Network(net.decode())
            return str(ip.netmask)
        return None

    @staticmethod
    def get_subnet_from_network(str):
        if NetworkObjectManipulation.is_network(str):
            ip = ipaddress.IPv4Network(str.decode())
            return str(ip.network_address)
        return None

    @staticmethod
    def is_dns_name(str):
        list_of_ips = NetworkObjectManipulation.get_ip_list_from_dns(str)
        if len(list_of_ips) > 0:
            return True
        return False

    @staticmethod
    def get_ip_list_from_dns(str):
        ip_list = []
        ais = socket.getaddrinfo(str)
        for result in ais:
            ip_list.append(result[-1][0])
        return ip_list

    @staticmethod
    def is_valid_protocol(str):
        if str == 'tcp' or str == 'udp':
            return True
        return False

    @staticmethod
    def is_valid_port(port):
        if isinstance(port, str) or isinstance(port, int):
            port = int(port)
            if port > 1 and port < 65535:
                return True
            return False
        return False




