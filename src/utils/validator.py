import json

from jsonschema import validate
from jsonschema import ValidationError
from logging import getLogger

logger = getLogger(__name__)


class Validator(object):

    @staticmethod
    def validate_json_by_schema_file(json_to_validate, schema_file):
        try:
            with open(schema_file, 'r') as schema:
                schema = json.load(schema)
            validate(json_to_validate, schema)
        except IOError:
            raise Exception("IOError Exception: could not open file {0}".format(schema_file))
        except ValidationError, e:
            raise Exception("ValidationError Exception: json is not valid. description: {}".format(e.message))
        except TypeError, e:
            logger.error("TypeError Exception: {0}".format(e.message))
        except Exception, e:
            raise Exception("Internal error occurred.")