
class UnknownEntityException(Exception):
    pass

class UnknownServiceException(Exception):
    pass

class PortOutOfRange(Exception):
    pass

class InvalidProtocol(Exception):
    pass

class InvalidGroupException(Exception):
    pass

class RequestFailed(Exception):
    pass

class TagNotFoundInCache(Exception):
    pass