import time
import os
import pika

SLEEP_TIME = 60*10

def scheduled_sync():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=os.environ['RABBIT_SERVER']))
    channel = connection.channel()
    channel.queue_declare(queue='redis_sync_trigger', durable=True)
    while True:
        print("Sending request to RabbitMQ")
        channel.basic_publish(exchange='',
                              routing_key='redis_sync_trigger',
                              body={},
                              properties=pika.BasicProperties(
                                  delivery_mode=2
                              ))
        print("Request Sent")
        time.sleep(SLEEP_TIME)

if __name__ == '__main__':
    scheduled_sync()

