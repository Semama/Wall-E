import json
import logging

from flask import Flask, make_response
from flask_cors import CORS

from src.configs import *
from src.view.views import CheckpointViews
from os import environ as ENV


class Server(Flask):

    def __init__(self, name):
        Flask.__init__(self, name)
        self.api = CheckpointViews()
        self.configRoutes()
        self.configLogger()

    def configRoutes(self):
        routes = self.api.get_api_routes()
        for route in routes:
            self.add_url_rule(route['rule'], view_func=route['function'], methods=route['methods'])

    @staticmethod
    def configLogger():
        logging.getLogger(__name__)
        logging.basicConfig(filename=logfile,
                            format='[%(asctime)s] [%(levelname)s] %(module)s - %(funcName)s:   %(message)s',
                            level=loglevel,
                            datefmt='%m/%d/%Y %I:%M:%S %p')

api_router = Server(__name__)
CORS(api_router, resources={r"/*":{"origins":"*"}})
#api_router.debug = True
ENV['FLASK_DEBUG'] = ENV.get('FLASK_DEBUG', 'False')
api_router.debug = ENV['FLASK_DEBUG']

@api_router.errorhandler(404)
def not_found(error):
    return make_response(json.dumps({'error': 'Not found'}), 404)

if __name__ == '__main__':
    print "Flask Server start listening.."
    api_router.run(host=host, port=int(port))
