import os
import pika
import redis
import json
import datetime
import ast

from flask import request
from flask.helpers import make_response
from logging import getLogger
from dateutil import parser

from definitions import CONFIG_DIR, REQUEST_TIMEOUT
from src.utils.validator import Validator
from src.utils.entities.request import Request

logger = getLogger(__name__)
requestSchemaFile = os.path.join(CONFIG_DIR, 'requestSchema.json')

class CheckpointViews(object):

    pool = redis.ConnectionPool(host=os.environ['REDIS_SERVER'], port=6379, db=0)
    redis = redis.Redis(connection_pool=pool)

    def hello(self):
        return make_response('hello world!!', 200)

    def get_service_protocols(self):
        return make_response(json.dumps({'protocols': ['tcp', 'udp']}), 200)

    def get_status(self, project):
        status = self._get_request_from_redis(project=project, field='status')
        if not status: return make_response("Project not found", 404)
        return make_response(json.dumps(status), 200)

    def get_description(self, project):
        desc = self._get_request_from_redis(project=project, field='description')
        if not desc: return make_response("Description not found", 404)
        return make_response(json.dumps(desc), 200)

    def get_request(self, project):
        data = self._get_request_from_redis(project=project, field='request')
        if not data: return make_response("Project not found", 404)
        data = Request.unwrap_entities_from_project_name(project_name=project, body=data)
        return make_response(json.dumps(data), 200)

    def get_projects(self):
        return make_response(json.dumps({'projects': CheckpointViews.redis.keys("[^" + Request.request_format + "]*")}), 200)

    def _get_request_from_redis(self, project, field=None):
        request_id = Request.request_format + str(project)
        exists = CheckpointViews.redis.exists(request_id)
        if exists:
            request = json.loads(CheckpointViews.redis.get(request_id))
            if not field: return request
            if field in request.keys(): return request[field]
        return None

    def install_policy(self):
        data = request.get_json(force=True)
        Validator.validate_json_by_schema_file(data, requestSchemaFile)
        req = Request(data, wrap=True)
        return self.send_request(req)

    def send_request(self, req):
        print("Handling request: {id}".format(id=req.id))
        should_insert = CheckpointViews.should_insert_a_new_request(req)
        if should_insert == True:
            print("Inserting new request for project: {}".format(req.id))
            self.send_request_to_redis(req)
            self.send_request_to_rabbit(req)
            return " x Request Sent."
        return "Request not Sent. Please check request status for more details"

    def get_project_policy(self, project):
        return self.get_project_policy(project)

    def send_request_to_rabbit(self, body, queue='checkpoint_requests'):
        print("Sending request to RabbitMQ")
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=os.environ['RABBIT_SERVER']))
        channel = connection.channel()
        channel.queue_declare(queue='checkpoint_requests', durable=True)
        channel.basic_publish(exchange='',
                              routing_key='checkpoint_requests',
                              body=str(body),
                              properties=pika.BasicProperties(
                                  delivery_mode=2
                              ))
        print("Request Sent")
        return "Request Sent."

    def send_request_to_redis(self, request):
        print("starting transaction to redis..")
        r = CheckpointViews.redis
        if r.exists(request.get_key()):
            r.delete(request.get_key())
        r.set(request.get_key(), str(request))

    @staticmethod
    def should_insert_a_new_request(req):
        print("shoud_insert_a_new_request function")
        should_insert_a_new_request = True
        key = req.get_key()
        is_exist = CheckpointViews.redis.exists(key)
        if is_exist:
            cached_data = json.loads(CheckpointViews.redis.get(key))
            body = cached_data['request']
            cached_request = Request(body)
            cached_request.status = cached_data['status']
            cached_request.created_time = cached_data['created_time']
            if cached_request.body == req.body:
                if cached_request.status == 'Done':
                    print("the request status is done.")
                    CheckpointViews.update_request_created_time(req=req, created_time=req.created_time)
                    if CheckpointViews._check_if_request_really_installed(req.body):
                        should_insert_a_new_request = False
                elif cached_request.status == 'Pending':
                    if (datetime.datetime.now() - parser.parse(cached_request.created_time)).total_seconds() > REQUEST_TIMEOUT:
                        cached_request.status = 'Failed'
                        CheckpointViews.redis.set(key, str(cached_request))
                    else:
                        should_insert_a_new_request = False
        return should_insert_a_new_request

    @staticmethod
    def _check_if_request_really_installed(request_body):
        print("inside the magical function")
        for entity_type in request_body.keys():
            key = "_" + entity_type
            if CheckpointViews.redis.exists(key):
                for entity in request_body[entity_type]:
                    if entity not in ast.literal_eval(CheckpointViews.redis.get(key)):
                        print("exited in type: {t}, object: {o}".format(t=entity_type, o=entity))
                        return False
            else:
                return False
        return True

    @staticmethod
    def update_request_created_time(req, created_time):
        cached_data = json.loads(CheckpointViews.redis.get(req.get_key()))
        req = Request(body=cached_data['request'])
        req.created_time = created_time
        req.status = cached_data['status']
        CheckpointViews.redis.set(req.get_key(), str(req))

    def get_api_routes(self):
        routes = [
            { 'rule': '/Hello',
              'function': self.hello,
              'methods': ['GET']},
            { 'rule': '/<project>/get_policy',
              'function': self.get_project_policy,
              'methods': ['GET']},
            { 'rule': '/install_policy',
              'function': self.install_policy,
              'methods': ['POST']},
            { 'rule': '/get_service_protocols',
              'function': self.get_service_protocols,
              'methods': ['GET']},
            { 'rule': '/<project>/get_status',
              'function': self.get_status,
              'methods': ['GET']},
            { 'rule': '/<project>/get_description',
              'function': self.get_description,
              'methods': ['GET']},
            { 'rule': '/<project>/get_request',
              'function': self.get_request,
              'methods': ['GET']},
            { 'rule': '/get_projects',
              'function': self.get_projects,
              'methods': ['GET']}
            ]
        return routes