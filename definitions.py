import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG_DIR = os.path.join(ROOT_DIR, 'conf')
CONFIG_PATH = os.path.join(CONFIG_DIR, 'checkpoint_automation.conf')
INSTALL_POLICY_TIMEOUT = 60*2
WALLE_TIMEOUT = 60*3
REQUEST_TIMEOUT = INSTALL_POLICY_TIMEOUT + WALLE_TIMEOUT