#!/bin/bash

echo "Destroy Environment.. "
docker service rm `docker service ls | grep walle | awk '{print $1}'`

# building images
echo "building new images.. "
docker build -t walle/view:latest -f View_Dockerfile .
docker build -t walle/controller:latest -f Controller_Dockerfile .
docker build -t walle/redis_sync:latest -f Redis_Sync_Dockerfile .
docker build -t walle/scheduled_redis_sync_task -f Scheduled_Redis_Sync_Task_Dockerfile .

# pushing to repository
echo "push to registry.. "
docker push walle/view
docker push walle/controller
docker push walle/redis_sync
docker push walle/scheduled_redis_sync_task


# deploy
echo "create new env.."
#docker network create -d overlay walle
docker service create --name=walle-rabbit --network=walle --replicas=1 geiger/rabbitmq:jess-ready
docker service create --name=walle-redis --network=walle --replicas=1 redis:4.0.2
sleep 7
docker service create --name=walle-view --network=walle --replicas=1 --env RABBIT_SERVER=walle-rabbit --env REDIS_SERVER=walle-redis -p 2493:2493 --detach=true walle/view:latest
docker service create --name=walle-controller --network=walle --replicas=1 --env RABBIT_SERVER=walle-rabbit --env REDIS_SERVER=walle-redis --detach=true walle/controller
docker service create --name=walle-redis-sync --network=walle --replicas=1 --env RABBIT_SERVER=walle-rabbit --env REDIS_SERVER=walle-redis --detach=true walle/redis_sync
docker service create --name=walle-scheduled_redis_sync_task --network=walle --replicas=1 --env RABBIT_SERVER=walle-rabbit --detach=true walle/scheduled_redis_sync_task

