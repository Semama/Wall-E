#!/bin/bash

docker service rm walle-view
docker build -t walle/view:latest -f View_Dockerfile .
docker push walle/view
docker service create --name=walle-view --network=walle --replicas=1 --env RABBIT_SERVER=walle-rabbit --env REDIS_SERVER=walle-redis -p 2493:2493 walle/view:latest


