#!/bin/bash

echo "Destroy Environment.. "
docker service rm walle-controller
docker build -t walle/controller:latest -f Controller_Dockerfile .
docker push walle/controller
docker service create --name=walle-controller --network=walle --replicas=1 --env RABBIT_SERVER=walle-rabbit --env REDIS_SERVER=walle-redis walle/controller

