import unittest

import mock

from src.checkpoint.checkpoint import Checkpoint

class TestCheckpointClass(unittest.TestCase):

    checkpointPath = 'src.checkpoint.entities.session'

    @mock.patch(checkpointPath + '.Session')
    def setUp(self, mock_session):
        mock_session.return_value = mock.Mock()
        self.checkpoint = Checkpoint()

    def test_login(self):
        # defining the correct return values of session.login() and session.get_token().
        self.checkpoint.session.login.return_value = True
        self.checkpoint.session.get_token.return_value = "Token"
        # try to login
        result = self.checkpoint.login()
        # check if checkpoint fields created and if checkpoint.login returned True.
        self.assertIsNotNone(self.checkpoint.endpoint)
        self.assertIsNotNone(self.checkpoint.policy)
        self.assertTrue(result)

    def test_login_shouldRaiseException_when_sessionLogin_RaiseException(self):
        self.checkpoint.session.login = Exception()
        with self.assertRaises(Exception):
            self.checkpoint.login()

    def test_logout(self):
        # there's nothing the check. but just for practice :)
        self.checkpoint.session.logout.return_value = True
        self.assertTrue(self.checkpoint.logout())

    def test_logout_shouldRaiseException_when_sessionLogout_RaiseException(self):
        self.checkpoint.session.logout = Exception()
        with self.assertRaises(Exception) as cm:
            self.checkpoint.logout()
