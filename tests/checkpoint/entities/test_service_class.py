import json
import unittest

import mock
import requests
import requests_mock

from src.checkpoint.entities.service import Service

class TestServiceClass(unittest.TestCase):

    servicePath = 'src.checkpoint.entities.service'

    def setUp(self):
        self.service = Service()
        self.adapter = requests_mock.Adapter()
        self.mock_url = 'mock://checkpoint'

    def test_