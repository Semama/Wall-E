import json
import unittest

import mock
import requests
import requests_mock

from src.checkpoint.entities.session import Session

class TestSessionClass(unittest.TestCase):

    sessionPath = 'src.checkpoint.entities.session'

    def setUp(self):
        self.session = Session()
        self.adapter = requests_mock.Adapter()
        self.mock_url = 'mock://checkpoint'

    @mock.patch(sessionPath + '.requests')
    def test_login_when_requestToCheckpoint_succeeded(self, mock_requests):
        mock_requests.post.return_value = self.create_checkpoint_mock_request('POST', text='{"sid":"Token"}', status_code=200)
        self.assertTrue(self.session.login())
        self.assertEqual(self.session.token, 'Token')

    @mock.patch(sessionPath + '.requests')
    def test_loging_when_requestToCheckpoint_failed(self, mock_requests):
        mock_requests.post.return_value = self.create_checkpoint_mock_request('POST', text='{"code":"error_name", "message":"description"}', status_code=400)
        with self.assertRaises(Exception) as cm:
            self.session.login()
        message = cm.exception.message
        self.assertEqual(message['code'], 'error_name')

    @mock.patch(sessionPath +'.requests')
    def test_loging_when_logged_in(self, mock_requests):
        mock_requests.post.return_value = self.create_checkpoint_mock_request('POST', text='{"sid":"Token"}', status_code=200)
        self.assertTrue(self.session.login())
        self.assertTrue(self.session.login())

    @mock.patch(sessionPath + '.requests')
    def test_logout_when_requestToCheckpoint_succeeded(self, mock_requests):
        mock_requests.post.return_value = self.create_checkpoint_mock_request('POST', text='{"message":"OK"}', status_code=200)
        self.session.is_logged_in = mock.MagicMock(return_value=True)
        self.assertTrue(self.session.logout())

    @mock.patch(sessionPath + '.requests')
    def test_logout_requestsToCheckpoint_failed(self, mock_requests):
        self.session.is_logged_in = mock.MagicMock(return_value=True)
        mock_requests.post.return_value = self.create_checkpoint_mock_request('POST', text='{"code":"error_name", "message":"description"}', status_code=400)
        with self.assertRaises(Exception) as cm:
            self.session.logout()
        message = cm.exception.message
        message = json.loads(message)
        self.assertEqual(message['code'], 'error_name')

    @mock.patch(sessionPath + '.requests')
    def test_logout_when_logged_out(self, mock_requests):
        mock_requests.post.return_value = self.create_checkpoint_mock_request('POST', text='{"message":"OK"}', status_code=200)
        self.session.is_logged_in = mock.MagicMock(return_value=True)
        self.assertTrue(self.session.logout())
        self.assertTrue(self.session.logout())

    def create_checkpoint_mock_request(self, method, text, status_code):
        s = requests.Session()
        s.mount('mock', self.adapter)
        self.adapter.register_uri(method, self.mock_url, text=text, status_code=status_code)
        return s.request(method=method, url=self.mock_url)

